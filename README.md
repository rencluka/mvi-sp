# Semestralni prace MI-MVI Picture Background Change (Modifikace Swap teams/flags)

Vystupem semestralni prace je aplikace, ktera pro vstupni obrazek prohodi pozadi za jine uzivatelem definovane.

## Assignment
S pomoci latky probirane na kurzu MI-MVI vypracujte aplikaci pro zamenu pozadi u fotografie. 

Input:

* puvodni obrazek
* nove pozadi 

Output:
* puvodni obrazek s novym pozadim

## Installation

Instalace neni zapotrebi, aplikace bezi pod Google Colaboratory
```link
 https://colab.research.google.com
```
Odkaz na projekt je uveden zde: [/sources/scr]

## Usage
[Zde](https://colab.research.google.com/drive/15Eyajip9gu3u1zDvHOsi-9kVP5zLlR9x?authuser=1#scrollTo=bK2sbSjXV4uT)
* Do "original" vlozte adresu vstupniho obrazku. 
* Do "background" vlozte adresu noveho pozadi. 
* Spustte kod pomoci Ctrl + F9. 


## License
[MIT](https://choosealicense.com/licenses/mit/)